package Objects;

public class Coin {

    private String number;
    private double value;

    public Coin(String number, double value) {

        this.number = number;
        this.value = value;

    }

    public double getValue() {
        return value;
    }

    public String getNumber() {
        return number;
    }
}