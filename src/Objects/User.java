package Objects;

import java.util.ArrayList;
import java.util.Scanner;

public class User {

    private ArrayList<Coin> coins;
    private double amount;

    public User() {

        coins = new ArrayList<>();
        amount = 0;

    }

    public ArrayList<Coin> getCoins() {
        return coins;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setCoins(ArrayList<Coin> coins) {
        this.coins = coins;
    }

    public void depositCoins() {

        System.out.println("\nInsert coins (5 ctms, 10 ctms, 20 ctms, 50 ctms, 1€, 2€)");
        System.out.println("\nTye: | 2 to insert 2€ | 1 to insert 1€ | 5 to insert 5 ctms ... -1 to stop depositing");

        Scanner sc = new Scanner(System.in);
        int value = 0;

        while (value != -1) {

            value = sc.nextInt();

            switch (value) {
                case 2:
                    coins.add(new Coin("2€" , 2));
                    amount += 2;
                    break;
                case 1:
                    coins.add(new Coin("1€" , 1));
                    amount += 1;
                    break;
                case 5:
                    coins.add(new Coin("5 ctms" , 0.05));
                    amount += 0.05;
                    break;
                case 10:
                    coins.add(new Coin("10 ctms" , 0.1));
                    amount += 0.1;
                    break;
                case 20:
                    coins.add(new Coin("20 ctms" , 0.2));
                    amount += 0.2;
                    break;
                case 50:
                    coins.add(new Coin("50 ctms" , 0.5));
                    amount += 0.5;
                    break;
                case -1:
                    break;
                default:
                    System.out.print("\nObjects.Coin not accepted\n");
            }

        }

    }

    public Item buyItem(ArrayList<Item> machineItems) {

        System.out.println("\nThis is the vending machine stock. Type number to select product: \n");

        for (int i = 0; i < machineItems.size(); i++) {

            Item item = machineItems.get(i);
            System.out.println("[" + i + "]" + " " + item.getProduct() + " " + item.getPrice() + " €");

        }

        Scanner sc = new Scanner(System.in);
        int selection = sc.nextInt();

        if(selection >= 0 && selection <= machineItems.size()) {

            Item item = machineItems.get(selection);

            if(amount >= item.getPrice()) {
                machineItems.remove(item);
                return item;
            } else {
                System.out.println("\nyou have not introduced enough money\n");
                return null;
            }

        } else {
            System.out.println("\nError getting de product. \n");
            return null;
        }

    }

    public void resetData() {
        coins.clear();
        amount = 0;
    }

}