package Objects;

public class Item {

    private String produt;
    private double price;

    public Item(String produt, double price) {

        this.produt = produt;
        this.price = price;

    }

    public double getPrice() {
        return price;
    }

    public String getProduct() {
        return produt;
    }
}
