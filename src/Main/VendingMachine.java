package Main;

import Objects.Coin;
import Objects.Item;
import Objects.User;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/*
 * This is the main class of the project. It contains a list of items (products of the vending machine)
 * and a list of coins, according to the program characteristics:

    You need to design a Vending Machine which
    - Accepts coins from 0.05€, 0.10€, 0.20€, 0.50€, 1€, 2€
    - Allow user to select products Coke(1.50€), Sprite(1.45€), Water(0.90€)
    - Allow user to take refund by canceling the request.
    - Return selected product and remaining change if any
    - Allow reset operation (amount of product stock & change) for vending machine supplier


    The program is also designed to manage the coins that the machine has, and the user change coins too.

*/

public class VendingMachine {

    private ArrayList<Item> items;
    private ArrayList<Coin> coins;
    private double amount = 0; // This variable saves the total machine amount. Directly relacionated with the coins

    public static void main(String args[]) {
        VendingMachine vendingMachine = new VendingMachine();
    }

    public VendingMachine() {

        items = new ArrayList<>();
        coins = new ArrayList<>();

        // This function sets up the products and the coins in the machine (It would be useful for the supplier)
        setInitialData();

        // We instantiate a user, which will make all the operations (insert money and buy items
        User user = new User();

        /*

        This is an example of a user deposit and a product buy process.

        user.depositCoins();

        Objects.Item purchased = user.buyItem(items);
        double price = purchased.getPrice();
        double userAmount = user.getAmount();
        ArrayList<Objects.Coin> userCoins = user.getCoins();

        // We save this information temporarily. If an operation cannot be performed we'll return
        // to the user his inserted money

        if (purchased != null) {

            // This process simulates when the vending machine would get the money for real
            // This is done in order to have the user's money when computing the change

            coins.addAll(user.getCoins());
            amount += user.getAmount();
            user.resetData();

            boolean canReturnChange = returnChange(user, price, userAmount);

            if (canReturnChange) {
                items.remove(purchased); // We give the product to the user
            }  else {

                amount -= userAmount;
                coins.removeAll(userCoins);
                user.setAmount(userAmount);
                user.setCoins(userCoins);

                // If there was a problem we return the credit to the user
                // Here we would have to warn the vending machine supplier

            }
        }

        */

    }

    /*
     * This function compute the amount of money that we have to return to the user, and
     * the respective coins.
    */

    public boolean returnChange(User user, double price, double userAmount) {

        double change = userAmount - price;
        double tempChange = change; // This variable will be decreased until it is equal to 0
        double remainingChange = 0; // This variable will be increased until it is equal to the change
        ArrayList<Coin> changeCoins = new ArrayList<>();

        coins.sort(Comparator.comparing(Coin::getValue));
        Collections.reverse(coins);

        // We sort and reverse the coins in order to check and give to the user
        // the maximum possible amount of "more value" coins.

        // First, we need to check if the machine has enough money to return the change to the user
        if(change <= amount) {
            while(remainingChange < change) {
                for (Coin coin : coins) {
                    if(coin.getValue() <= tempChange) {
                        remainingChange += coin.getValue();
                        tempChange -= coin.getValue();
                        changeCoins.add(coin);
                        break;
                    }

                }
            }

            // We remove the change coins of the machine, and we give it to the user
            coins.removeAll(changeCoins);
            user.setAmount(remainingChange);
            user.setCoins(changeCoins);

            return true;

        }

        return false;

    }


    public void setInitialData() {

        items = populateItems();
        coins = populateCoins();
        amount = 22;

    }

    private ArrayList<Item> populateItems() {

        ArrayList<Item> list = new ArrayList<>();

        list.add(new Item("Coke", 1.5));
        list.add(new Item("Coke", 1.5));
        list.add(new Item("Coke", 1.5));
        list.add(new Item("Coke", 1.5));
        list.add(new Item("Coke", 1.5));
        list.add(new Item("Coke", 1.5));
        list.add(new Item("Coke", 1.5));
        list.add(new Item("Coke", 1.5));
        list.add(new Item("Coke", 1.5));
        list.add(new Item("Coke", 1.5));

        list.add(new Item("Sprite", 1.45));
        list.add(new Item("Sprite", 1.45));
        list.add(new Item("Sprite", 1.45));
        list.add(new Item("Sprite", 1.45));
        list.add(new Item("Sprite", 1.45));
        list.add(new Item("Sprite", 1.45));
        list.add(new Item("Sprite", 1.45));
        list.add(new Item("Sprite", 1.45));
        list.add(new Item("Sprite", 1.45));
        list.add(new Item("Sprite", 1.45));

        list.add(new Item("Water", 0.9));
        list.add(new Item("Water", 0.9));
        list.add(new Item("Water", 0.9));
        list.add(new Item("Water", 0.9));
        list.add(new Item("Water", 0.9));
        list.add(new Item("Water", 0.9));
        list.add(new Item("Water", 0.9));
        list.add(new Item("Water", 0.9));
        list.add(new Item("Water", 0.9));
        list.add(new Item("Water", 0.9));

        return list;

    }

    private ArrayList<Coin> populateCoins() {

        ArrayList<Coin> list = new ArrayList<>();

        list.add(new Coin("1€" , 1));
        list.add(new Coin("1€" , 1));
        list.add(new Coin("1€" , 1));
        list.add(new Coin("1€" , 1));
        list.add(new Coin("1€" , 1));
        list.add(new Coin("1€" , 1));
        list.add(new Coin("1€" , 1));
        list.add(new Coin("1€" , 1));
        list.add(new Coin("1€" , 1));
        list.add(new Coin("1€" , 1));

        list.add(new Coin("50 ctms" , 0.5));
        list.add(new Coin("50 ctms" , 0.5));
        list.add(new Coin("50 ctms" , 0.5));
        list.add(new Coin("50 ctms" , 0.5));
        list.add(new Coin("50 ctms" , 0.5));
        list.add(new Coin("50 ctms" , 0.5));
        list.add(new Coin("50 ctms" , 0.5));
        list.add(new Coin("50 ctms" , 0.5));
        list.add(new Coin("50 ctms" , 0.5));
        list.add(new Coin("50 ctms" , 0.5));


        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));
        list.add(new Coin("20 ctms" , 0.2));

        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));
        list.add(new Coin("10 ctms" , 0.1));


        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));
        list.add(new Coin("5 ctms" , 0.05));

        return list;

    }

}
